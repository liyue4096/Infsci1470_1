using UnityEngine;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    // Update is called once per frame
    void Update()
    {
        
    }

    public void FadeToLevel (int levelIndex)
    {
        animator.SetTrigger("FadeOut");
    }

    public void RespondToSignal()
    {
        animator.SetTrigger("FadeOut");
        Debug.Log("Signal received!");
    }
}
