using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SceneToGameplay : MonoBehaviour
{
    public PlayableDirector Timeline;
    public GameObject cutsceneCamera;
    public GameObject MainCamera;
    public GameObject Adventurer;
    public GameObject Panel;
    public MonoBehaviour[] gameplayScripts;

    void Start()
    {
        foreach (var script in gameplayScripts)
        {
            script.enabled = false;
        }

        Timeline.stopped += OnCutsceneFinished;
    }

    void OnCutsceneFinished(PlayableDirector director)
    {
        if (director == Timeline)
        {
            TransitionToGameplay();
        }
    }

    public void TransitionToGameplay()
    {
        if (cutsceneCamera != null)
        {
            cutsceneCamera.SetActive(false);
        }

        if (MainCamera != null)
        {
            MainCamera.SetActive(true);
        }

        foreach (var script in gameplayScripts)
        {
            if (script != null)
            {
                script.enabled = true;
            }
        }

        if (Timeline != null)
        {
            Timeline.gameObject.SetActive(false);
        }

        if (Panel != null)
        {  
            Panel.gameObject.SetActive(false); 
        }
    }
}