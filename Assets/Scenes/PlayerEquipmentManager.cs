using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipmentManager : MonoBehaviour
{
    public GameObject shield; // Assign this in the Inspector

    public void EquipShield()
    {
        Debug.Log("shield SetActive");
        shield.SetActive(true);
    }
    // Start is called before the first frame update
    void Start()
    {
        shield.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
