using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Required for managing scenes

public class TotemController : MonoBehaviour
{
    public float rotationSpeed = 720f; // Degrees per second
    public LayerMask playerLayer; // Assign this in the Inspector
    public float rayLength = 10f;
    public int damage = 20; // Example damage value
    private LineRenderer lineRenderer;
    private Dictionary<GameObject, int> playerHits = new Dictionary<GameObject, int>(); // Tracks the number of times each player is hit
    public GameObject WinPanel; // Assign this in the Inspector

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        // Optionally initialize line renderer properties
        lineRenderer.startColor = Color.green;
        lineRenderer.endColor = Color.green;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.positionCount = 2; // A line has two points: start and end
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);

        Vector3 rayDirection = transform.TransformDirection(new Vector3(0, 1, 0).normalized);

        
        RaycastHit hit;
        Collider TotemCollider = GetComponent<Collider>();
        Vector3 rayStartPosition = TotemCollider.bounds.center; // Center of the collider

        // Emit ray from monster's position forward
        Ray ray = new Ray(rayStartPosition, rayDirection);


        if (Physics.Raycast(ray, out hit, rayLength, playerLayer))
        {
            //Debug.DrawRay(transform.position, transform.forward * rayLength, Color.red); // For debugging
                                                                                         // Check if the ray has hit the player
            if (hit.collider.CompareTag("Player"))
            {
                HandlePlayerDamage(hit.collider.gameObject);
                //// Increment hit count for this player
                //if (!playerHits.ContainsKey(hit.collider.gameObject))
                //{
                //    playerHits[hit.collider.gameObject] = 1; // First hit
                //}
                //else
                //{
                //    playerHits[hit.collider.gameObject] += 1;
                //}

                //// Check if the player has been hit twice
                //if (playerHits[hit.collider.gameObject] == 2)
                //{
                //    // Call a method to handle player death
                //    HandlePlayerDamage(hit.collider.gameObject);
                //    playerHits[hit.collider.gameObject] = 0; // Reset hit count or remove the player from the dictionary if they are dead
                //}
            }
        }

        // Update Line Renderer to draw the ray
        // Start point is the monster's position, end point is in the direction of the ray and extends to rayLength
        lineRenderer.SetPosition(0, rayStartPosition - rayDirection * rayLength);
        lineRenderer.SetPosition(1, rayStartPosition + rayDirection * rayLength);
    }

    void HandlePlayerDamage(GameObject player)
    {
        // Attempt to get the PlayerHealth component on the player GameObject
        PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();

        // If the component exists, call the Die method
        if (playerHealth != null)
        {
            playerHealth.HandlePlayerDamage();
        }
    }

    // OnTriggerEnter is called when another Collider enters the trigger
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Make sure your player GameObject is tagged as "Player"
        {
            StartCoroutine(HandleWinCondition());
            // Show the "You Win" panel
            //if (WinPanel != null)
            //{
            //    WinPanel.SetActive(true);
            //    Time.timeScale = 0;
            //}
        }
    }

    IEnumerator HandleWinCondition()
    {
        // Optionally show a win panel if you have a UI setup for it
        if (WinPanel != null)
        {
            WinPanel.SetActive(true);
        }

        // Pause the game for 1 second
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(1); // Wait for 1 second in real time, ignoring the time scale

        // Resume time scale if needed or directly load the new scene
        Time.timeScale = 1;
        //Debug.Log("load scences2");
        // Change to Scene 2
        SceneManager.LoadScene("Scenes2"); // Replace "Scene2" with the actual name or index of your scene
    }
    //void ApplyDamage(GameObject player)
    //{
    //    // Assuming the player has a script with a method to reduce health
    //    PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
    //    if (playerHealth != null)
    //    {
    //        playerHealth.TakeDamage(damage);
    //    }
    //}
}
