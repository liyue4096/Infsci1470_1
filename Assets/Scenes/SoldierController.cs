using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEditor.Searcher.SearcherWindow.Alignment;

public class SoldierController : MonoBehaviour
{
    private CharacterController _charactor;
    private Animator _animator;
    //public GameObject loseMessageCanvas;
    public GameObject loseMessagePanel;
    public float Rotatespeed = 2;
    public bool isDie;

    public float speed = 1f;
    public float jumpForce = 5f;
    private Rigidbody rb;
    private bool isGrounded;
    private bool hasDied = false;
    //static int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        _charactor = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _animator.SetBool("isDefend", false);
        rb = GetComponent<Rigidbody>();
        isDie = _animator.GetBool("isDie");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.V) || _animator.GetBool("isDie") && !hasDied)
        {
            _animator.SetBool("isDie", true);
            hasDied = true; // Prevents further death triggers
            Debug.Log("player died");
            PlayDeathAnimation();
            return;
            //isGrounded = false; // Prevents multi-jumping
        }

        float horizontal = Input.GetAxis("Horizontal");
        //Vector3 movement = new Vector3(horizontal, 0, 0) * moveSpeed;
        //rb.velocity = new Vector3(movement.x, rb.velocity.y, rb.velocity.z);

        float vertical = Input.GetAxis("Vertical");

        float X = Input.GetAxis("Mouse X") * Rotatespeed;
        transform.Rotate(0, X, 0);

        //float speed = Input.GetAxis("Vertical");
        //_animator.SetFloat("Speed", Mathf.Abs(speed));
        Vector3 dir = new Vector3(horizontal, 0, vertical);
        //transform.rotation = Quaternion.LookRotation(dir);
        // Check for right mouse button click
        if (Input.GetMouseButtonDown(1)) // 1 is the index for the right mouse button
        {
            // Enter defense posture
            //Debug.Log("Defense Activated");
            _animator.SetBool("isDefend", true);
        }
        else if (Input.GetMouseButtonUp(1)) // When the right mouse button is released
        {
            // Exit defense posture
            _animator.SetBool("isDefend", false);
        }

        if (Input.GetMouseButtonDown(0)) //
        {
            _animator.SetBool("isAttack", true);
        }
        else if (Input.GetMouseButtonUp(0)) // 
        {
            // Exit defense posture
            _animator.SetBool("isAttack", false);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            _animator.SetBool("isJump", true);
            //isGrounded = false; // Prevents multi-jumping
        }
        else
        {
            _animator.SetBool("isJump", false);
        }

        if (dir != Vector3.zero)
        {
            _animator.SetBool("isRun", true);
        }
        else
        {
            _animator.SetBool("isRun", false);
            //_animator.SetBool("isSideStep", false); // Ensure isSideStep is false when not moving
        }

        //Move_Update();
    }

    void FixedUpdate()
    {
        //Rigidbody rb = GetComponent<Rigidbody>();

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        // Create a movement vector in local space
        Vector3 movement = (transform.forward * moveVertical + transform.right * moveHorizontal).normalized;

        if (movement.magnitude > 0.1f)  // Checking for dead zone
        {
            // Add force in the direction the player is currently facing
            rb.AddForce(movement * speed, ForceMode.VelocityChange);
        }
        else
        {
            // Explicitly stop the Rigidbody if input magnitude is very low
            rb.velocity = Vector3.zero;
        }   
    }

    // Check if the player is grounded
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground")) // Ensure your ground objects are tagged as "Ground"
        {
            isGrounded = true;
        }
    }

    private void PlayDeathAnimation()
    {
        //_animator.SetTrigger("isDie"); // Trigger death animation
        //_animator.SetBool("isDie", false);
        StartCoroutine(ReloadSceneAfterDelay());
    }

    IEnumerator ReloadSceneAfterDelay()
    {

        this.enabled = false; // Optionally disable this script to stop other updates
        yield return new WaitForSeconds(1f);
        loseMessagePanel.SetActive(true);
        //loseMessageCanvas.SetActive(true);
        Debug.Log("start reload scene");
        yield return new WaitForSeconds(2f); // Wait for the length of the death animation
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // Reload the current scene
    }

    private void Move_Update()
    {
        float horizontal = Input.GetAxis("Horizontal");

        float vertical = Input.GetAxis("Vertical");

        Vector3 dir = new Vector3(horizontal, 0, vertical);
        if(dir != Vector3.zero)
        {
            _animator.SetBool("isWalk", true);
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.forward * 2 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(Vector3.back * 2 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * 2 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.A) && (Input.GetKey(KeyCode.A)))
            {
                transform.Translate(Vector3.left * 2 * Time.deltaTime);
            }
        }
        else
        {
            _animator.SetBool("isWalk", false);
        }
        //transform.rotation = Quaternion.LookRotation(dir);

        
    }
}
