using UnityEngine;

public class AdventurerController : MonoBehaviour
{
    public float walkSpeed;
    public float runSpeed;
    public Transform cameraTransform;
    private Animator animator;
    private Rigidbody rb;
    private bool isGrounded;
    public float groundCheckDistance = 0.2f;
    public LayerMask groundLayer;
    //public GameObject swordInHand;
    //public Collider swordCollider;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        //swordInHand.SetActive(false);
        //animator.ResetTrigger("Attack");
        //swordCollider.enabled = false;
    }

    void Update()
    {
        GroundCheck();
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        bool isShiftPressed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        Vector3 inputDirection = new Vector3(horizontal, 0, vertical).normalized;

        bool isMoving = inputDirection.magnitude > 0.1f;
        //animator.SetBool("isWalking", isMoving && !isShiftPressed);
        animator.SetBool("isRun", isMoving && isShiftPressed);

        if (isMoving && isGrounded)
        {
            MoveCharacter(horizontal, vertical, isShiftPressed);
        }

        if (!isGrounded)
        {
            rb.AddForce(Vector3.down * 20f, ForceMode.Acceleration);
        }

        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("isAttack");
        }

        bool isAttacking = animator.GetCurrentAnimatorStateInfo(0).IsName("isAttack");
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        bool isShiftPressed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        Vector3 inputDirection = new Vector3(horizontal, 0, vertical).normalized;

        bool isMoving = inputDirection.magnitude > 0.1f;
        //animator.SetBool("isWalking", isMoving && !isShiftPressed);
        animator.SetBool("isRun", isMoving && isShiftPressed);

        if (isMoving)
        {
            float currentSpeed = isShiftPressed ? runSpeed : walkSpeed;
            Vector3 forward = cameraTransform.forward;
            Vector3 right = cameraTransform.right;

            forward.y = 0;
            right.y = 0;

            Vector3 moveDirection = (forward * vertical + right * horizontal).normalized;
            Quaternion rotation = Quaternion.LookRotation(moveDirection);
            rb.MoveRotation(rotation);

            Vector3 newPosition = rb.position + moveDirection * currentSpeed * Time.fixedDeltaTime;
            rb.MovePosition(newPosition);
        }

        if (isMoving && isGrounded)
        {
            MoveCharacter(horizontal, vertical, isShiftPressed);
        }

        if (!isGrounded)
        {
            rb.AddForce(Vector3.down * 800f, ForceMode.Acceleration);
        }
    }

    private void GroundCheck()
    {
        RaycastHit hit;
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, out hit, groundCheckDistance, groundLayer);
    }

    private void MoveCharacter(float horizontal, float vertical, bool isShiftPressed)
    {
        float currentSpeed = isShiftPressed ? runSpeed : walkSpeed;
        Vector3 forward = cameraTransform.forward;
        Vector3 right = cameraTransform.right;

        forward.y = 0;
        right.y = 0;

        Vector3 moveDirection = (forward * vertical + right * horizontal).normalized;
        Quaternion rotation = Quaternion.LookRotation(moveDirection);
        rb.MoveRotation(rotation);

        Vector3 newPosition = rb.position + moveDirection * currentSpeed * Time.fixedDeltaTime;
        rb.MovePosition(newPosition);
    }
}