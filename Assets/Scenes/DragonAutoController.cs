using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragonAutoController : MonoBehaviour
{
    public Transform playerTransform;
    public float moveSpeed = 5.0f;
    public float attackRange = 5.0f;
    public float warningRange = 35.0f;
    public float attackCooldown = 2.0f;
    public Animator playerAnimator;
    private float lastAttackTime = 0.0f;
    private Animator anim;
    public GameObject winMessage; // Assign a UI GameObject for the win message in the Inspector
    public int hp = 10; // Dragon can be hit 10 times before defeat
    bool isdead = false;
    private bool isApplyingForce = true;
    public Text healthDisplay; // Reference to the Text UI component

    int IdleSimple;
    int IdleAgressive;
    int IdleRestless;
    int Walk;
    int BattleStance;
    int Bite;
    int Drakaris;
    int FlyingFWD;
    int FlyingAttack;
    int Hover;
    int Lands;
    int TakeOff;
    int Die;
    bool isAttacking;
    Rigidbody rb;

    private void Start()
    {
        anim = GetComponent<Animator>();
        IdleSimple = Animator.StringToHash("IdleSimple");
        IdleAgressive = Animator.StringToHash("IdleAgressive");
        IdleRestless = Animator.StringToHash("IdleRestless");
        Walk = Animator.StringToHash("Walk");
        BattleStance = Animator.StringToHash("BattleStance");
        Bite = Animator.StringToHash("Bite");
        Drakaris = Animator.StringToHash("Drakaris");
        FlyingFWD = Animator.StringToHash("FlyingFWD");
        FlyingAttack = Animator.StringToHash("FlyingAttack");
        Hover = Animator.StringToHash("Hover");
        Lands = Animator.StringToHash("Lands");
        TakeOff = Animator.StringToHash("TakeOff");
        Die = Animator.StringToHash("Die");
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {

        if (!isdead)
        {
            MoveTowardsPlayer();
            if (Vector3.Distance(transform.position, playerTransform.position) <= attackRange)
            {
                AttackPlayer();
            }
        }
        else
        {
            DoDie();
        }
    }

    private void MoveTowardsPlayer()
    {
        if (transform.position.y > 0)
        {
            float downwardForce = 10f; // Adjust this value as needed
            rb.AddForce(Vector3.down * downwardForce, ForceMode.VelocityChange);
        }
        // Automatically move back to y = 0 if below 0
        if (transform.position.y < 0)
        {
            Vector3 correctedPosition = transform.position;
            correctedPosition.y = 0;
            transform.position = correctedPosition;
        }
        if (isApplyingForce) {
            // Move towards the player if not in attack range
            if (Vector3.Distance(transform.position, playerTransform.position) > attackRange && Vector3.Distance(transform.position, playerTransform.position) < warningRange)
            {
                isAttacking = false;
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSimple"))
                {
                    anim.SetBool(IdleSimple, false);
                    anim.SetBool(IdleAgressive, false);
                    anim.SetBool(IdleRestless, false);
                    anim.SetBool(Walk, true);
                    anim.SetBool(BattleStance, false);
                    anim.SetBool(Bite, false);
                    anim.SetBool(Drakaris, false);
                    anim.SetBool(FlyingFWD, false);
                    anim.SetBool(FlyingAttack, false);
                    anim.SetBool(Hover, false);
                    anim.SetBool(Lands, false);
                    anim.SetBool(TakeOff, false);
                    anim.SetBool(Die, false);
                }
                Vector3 direction = (playerTransform.position - transform.position).normalized;
                // Ensure the dragon only rotates around the y-axis
                direction.y = 0;

                // Rotate the dragon to face the player
                if (direction != Vector3.zero)
                {
                    Quaternion lookRotation = Quaternion.LookRotation(direction);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * moveSpeed);
                }
                //transform.position += direction * moveSpeed * Time.deltaTime;
                // Apply a force to move the dragon

                //rb.MovePosition(rb.position + direction * moveSpeed * Time.deltaTime);
                rb.AddForce(direction * moveSpeed * Time.deltaTime, ForceMode.VelocityChange);
            }
            else if (Vector3.Distance(transform.position, playerTransform.position) >= warningRange)
            {
                isAttacking = false;
                anim.SetBool(IdleSimple, true);
                anim.SetBool(IdleAgressive, false);
                anim.SetBool(IdleRestless, false);
                anim.SetBool(Walk, false);
                anim.SetBool(BattleStance, false);
                anim.SetBool(Bite, false);
                anim.SetBool(Drakaris, false);
                anim.SetBool(FlyingFWD, false);
                anim.SetBool(FlyingAttack, false);
                anim.SetBool(Hover, false);
                anim.SetBool(Lands, false);
                anim.SetBool(TakeOff, false);
                anim.SetBool(Die, false);
            }
            else
            {
                isAttacking = false;
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                {
                    anim.SetBool(IdleSimple, true);
                    anim.SetBool(IdleAgressive, false);
                    anim.SetBool(IdleRestless, false);
                    anim.SetBool(Walk, false);
                    anim.SetBool(BattleStance, false);
                    anim.SetBool(Bite, false);
                    anim.SetBool(Drakaris, false);
                    anim.SetBool(FlyingFWD, false);
                    anim.SetBool(FlyingAttack, false);
                    anim.SetBool(Hover, false);
                    anim.SetBool(Lands, false);
                    anim.SetBool(TakeOff, false);
                    anim.SetBool(Die, false);
                }
            }
        }
    }

    private void AttackPlayer()
    {
        if (!isdead) {
            // Only attack if cooldown has elapsed
            if (Time.time > lastAttackTime + attackCooldown)
            {
                Vector3 direction = (playerTransform.position - transform.position).normalized;
                // Ensure the dragon only rotates around the y-axis
                direction.y = 0;

                // Rotate the dragon to face the player
                if (direction != Vector3.zero)
                {
                    Quaternion lookRotation = Quaternion.LookRotation(direction);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * moveSpeed);
                }

                isAttacking = true;
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Bite"))
                {              //T to bite
                    anim.SetBool(IdleSimple, true);
                    anim.SetBool(IdleAgressive, false);
                    anim.SetBool(IdleRestless, false);
                    anim.SetBool(Walk, false);
                    anim.SetBool(BattleStance, false);
                    anim.SetBool(Bite, false);
                    anim.SetBool(Drakaris, false);
                    anim.SetBool(FlyingFWD, false);
                    anim.SetBool(FlyingAttack, false);
                    anim.SetBool(Hover, false);
                    anim.SetBool(Lands, false);
                    anim.SetBool(TakeOff, false);
                    anim.SetBool(Die, false);
                }
                else if (anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSimple"))
                {
                    anim.SetBool(IdleSimple, false);
                    anim.SetBool(IdleAgressive, false);
                    anim.SetBool(IdleRestless, false);
                    anim.SetBool(Walk, false);
                    anim.SetBool(BattleStance, false);
                    anim.SetBool(Bite, true);
                    anim.SetBool(Drakaris, false);
                    anim.SetBool(FlyingFWD, false);
                    anim.SetBool(FlyingAttack, false);
                    anim.SetBool(Hover, false);
                    anim.SetBool(Lands, false);
                    anim.SetBool(TakeOff, false);
                    anim.SetBool(Die, false);
                    lastAttackTime = Time.time;
                }
                //anim.SetTrigger("Bite"); // Assuming 'Bite' is the attack animation
            }
        }
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;
        anim.SetTrigger("Hit"); // Assuming 'Hit' triggers a flinch or damage reaction animation
        if (hp <= 0)
        {
            DoDie();
        }
    }

    private IEnumerator ResetAnimationStateAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        isApplyingForce = true; // Resume force application
        anim.SetBool(IdleSimple, true); // Reset to Idle or any other initial animation state
    }

    private void DoDie()
    {
        if (!isdead)
        {
            isdead = true;
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Bite"))
            {
                anim.SetBool(IdleSimple, false);
                anim.SetBool(IdleAgressive, false);
                anim.SetBool(IdleRestless, false);
                anim.SetBool(Walk, false);
                anim.SetBool(BattleStance, false);
                anim.SetBool(Bite, false);
                anim.SetBool(Drakaris, false);
                anim.SetBool(FlyingFWD, false);
                anim.SetBool(FlyingAttack, false);
                anim.SetBool(Hover, false);
                anim.SetBool(Lands, false);
                anim.SetBool(TakeOff, false);
                anim.SetBool(Die, true);
            }
            // Disable further movement and attacks
            else
            {
                anim.SetBool(IdleSimple, false);
                anim.SetBool(IdleAgressive, false);
                anim.SetBool(IdleRestless, false);
                anim.SetBool(Walk, false);
                anim.SetBool(BattleStance, false);
                anim.SetBool(Bite, false);
                anim.SetBool(Drakaris, false);
                anim.SetBool(FlyingFWD, false);
                anim.SetBool(FlyingAttack, false);
                anim.SetBool(Hover, false);
                anim.SetBool(Lands, false);
                anim.SetBool(TakeOff, false);
                anim.SetBool(Die, false);
            }
            this.enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("collision!");
        if (other.gameObject.CompareTag("Weapon") && playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("attack"))
        {
            hp--; // Decrease health by one on each collision
            Debug.Log("attack Smug!");
            UpdateHealthDisplay();
            if (hp <= 0)
            {
                DoDie();
            }
        }

        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(ResetAnimationStateAfterDelay(0.1f));
            isApplyingForce = false; // Stop applying forces temporarily
        }
        if (isAttacking && other.gameObject.CompareTag("Player"))
        {
            HandlePlayerDamage(other.gameObject);
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collision!");
        // Check if the collision is with the weapon and the player is in the attack animation
        if (collision.gameObject.CompareTag("Weapon") && playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("attack"))
        {
            hp--; // Decrease health by one on each collision
            Debug.Log("attack Smug!");
            UpdateHealthDisplay();
            if (hp <= 0)
            {
                DoDie();
            }
        }
    }


    void HandlePlayerDamage(GameObject player)
    {
        // Attempt to get the PlayerHealth component on the player GameObject
        PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();

        // If the component exists, call the Die method
        if (playerHealth != null)
        {
            playerHealth.HandlePlayerDamage();
        }
    }

    private void UpdateHealthDisplay()
    {
        if (healthDisplay != null)
            healthDisplay.text = "Smug HP: " + hp; // Update the text to reflect current HP
    }
}
