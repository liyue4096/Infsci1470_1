using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.UI;

public class InteractWithBox : MonoBehaviour
{
    public Transform playerTransform;
    public Transform boxTransform; // Assign this in the Inspector
    public GameObject interactionText; // Assign the UI Text GameObject you created
    public GameObject monster;
    public float interactionDistance = 0.5f;
    private bool isBoxOpened = false;
    public GameObject dialoguePanel; // Assign this in the Inspector
    public Text dialogueText; // Assign this in the Inspector
    public Button continueButton; // Assign this in the Inspector

    public Vector3 playerResetPosition = new Vector3(5.7f, 0, 112.2f);
    public Vector3 playerResetFacingDirection = new Vector3(0, 125, 0); // The Euler angles for reset facing direction


    void Start()
    {
        interactionText.SetActive(false); // Ensure interaction text is initially hidden
        dialoguePanel.SetActive(false);
        continueButton.onClick.AddListener(ResumeGame);
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(playerTransform.position, boxTransform.position) < interactionDistance)
        {
            // If within interaction distance, show the interaction text
            interactionText.SetActive(true);

            // Check if the player presses the F key
            if (Input.GetKeyDown(KeyCode.F) && !isBoxOpened)
            {
                // Code to "open" the box or perform an action
                // Hide the interaction text as the box is now "opened"
                interactionText.SetActive(false);

                // Disable the box to simulate it being opened
                boxTransform.gameObject.SetActive(false);

                // Activate the monster
                monster.SetActive(true);

                // Move the player back to the specified reset position
                playerTransform.position = playerResetPosition;
                // Set the player's facing direction
                playerTransform.rotation = Quaternion.Euler(playerResetFacingDirection);


                isBoxOpened = true;
                PauseGame();
                Debug.Log("Box Opened"); // Placeholder action
            }
        }
        else
        {
            // If not within interaction distance, hide the interaction text
            interactionText.SetActive(false);
        }
    }

    void PauseGame()
    {
        Time.timeScale = 0; // Pause the game
        dialogueText.text = "You have opened the box! Danger lurks ahead.";
        dialoguePanel.SetActive(true); // Show dialogue
    }

    void ResumeGame()
    {
        Time.timeScale = 1; // Resume the game
        dialoguePanel.SetActive(false);
        Debug.Log("Game Resumed");
    }
}
