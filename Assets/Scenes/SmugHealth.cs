using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmugHealth : MonoBehaviour
{
    private Animator _animator;
    public bool isDie = false;
    public int hp = 10;
    public Text healthDisplay; // Reference to the Text UI component

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void UpdateHealthDisplay()
    {
        if (healthDisplay != null)
            healthDisplay.text = "HP: " + hp; // Update the text to reflect current HP
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0)
        {
            Die();
        }
    }

    public void HandleSmugDamage()
    {

        hp--;
        UpdateHealthDisplay();

        if (hp <= 0) { Die(); }

    }

    public void Die()
    {
        isDie = true;
        _animator.SetBool("Die", true);
        // Any additional death logic (e.g., playing a death animation)
        //Debug.Log(gameObject.name + " has died.");
    }
}
