using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInteraction : MonoBehaviour
{
    public GameObject tipText; // Assign your UI Text or TextMeshPro element in the Inspector
    private bool playerInRange = false; // To check if the player is within the interaction range

    private void Update()
    {
        
        // Check if player is in range and presses the 'F' key
        if (playerInRange && Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("take weapon");
            tipText.SetActive(false); // Optionally hide the interaction text

            // Optionally deactivate or destroy the pickup object to prevent re-pickup
            gameObject.SetActive(false);
            //EquipItem();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Make sure your player GameObject is tagged as "Player"
        {
            tipText.SetActive(true);
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tipText.SetActive(false);
            playerInRange = false;
        }
    }

    void EquipItem()
    {
        Debug.Log("Equipping item...");

        // Example: Deactivate the equipment GameObject to simulate picking it up
        gameObject.SetActive(false);

        // Here, you would add your logic to:
        // 1. Add the item to the player's inventory
        // 2. Apply any effects or changes the item has on the player (e.g., change appearance, increase stats)

        // Optionally, you might want to notify the player that the item has been equipped
        // This can be done through the UI or any other method you see fit
    }
}
