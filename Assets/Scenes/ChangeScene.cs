using   System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    FadeInOut fade;
    // Start is called before the first frame update
    void Start()
    {
        fade = FindObjectOfType<FadeInOut>();
    }

    public IEnumerator _ChangeScene()
    {
        fade.FadeOut();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Scenes1");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Armature")
        {
            StartCoroutine(_ChangeScene());
            //SceneManager.LoadScene("Scenes1");
        }
    }
}
