using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShildInteraction : MonoBehaviour
{
    public GameObject tipText; // Assign your UI Text or TextMeshPro element in the Inspector
    private bool playerInRange = false; // To check if the player is within the interaction range
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Check if player is in range and presses the 'F' key
        if (playerInRange && Input.GetKeyDown(KeyCode.F))
        {
            tipText.SetActive(false);
            Debug.Log("take shield");
            PlayerEquipmentManager equipmentManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerEquipmentManager>();
            if (equipmentManager != null)
            {
                Debug.Log("EquipShield");
                equipmentManager.EquipShield();

                // Optionally deactivate or destroy the pickup object to prevent re-pickup
                gameObject.SetActive(false);
            }
            //EquipItem();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Make sure your player GameObject is tagged as "Player"
        {
            tipText.SetActive(true);
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tipText.SetActive(false);
            playerInRange = false;
        }
    }
}
