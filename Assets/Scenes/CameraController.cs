using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Searcher.SearcherWindow.Alignment;
//using UnityEngine.Windows;

public class Camera : MonoBehaviour
{
    private Camera m_Camera;
    public Transform playerTransform; // Reference to the player's transform
    //public Rigidbody rd;
    public float Rotatespeed = 1;
    private Vector3 offset; // Initial offset from the player

    // Start is called before the first frame update
    void Start()
    {
        m_Camera = GetComponent<Camera>();
        offset = transform.position - playerTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //float h = Input.GetAxis("Horizontal");
        //float v = Input.GetAxis("Vertical");
        float X = Input.GetAxis("Mouse X") * Rotatespeed;
        //float Y = Input.GetAxis("Mouse Y") * Rotatespeed;

        //m_Camera.transform.localRotation *= Quaternion.Euler(-Y, 0, 0);
        //transform.localRotation *= Quaternion.Euler(0, X, 0);
        transform.RotateAround(playerTransform.position, Vector3.up, X);

        // After rotation, reset the distance to the player
        transform.position = playerTransform.position + offset;

        // Re-calculate the new offset after rotation
        offset = transform.position - playerTransform.position;
    }
}
