using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementInstruction : MonoBehaviour
{
    public Text textToShow; 
    public float duration = 5f;

    // Start is called before the first frame update
    void Start()
    {
        if (textToShow != null)
        {
            textToShow.gameObject.SetActive(true); // Make sure the text is visible
            Invoke("HideText", duration); // Schedule the hiding of the text
        }
    }

    void HideText()
    {
        textToShow.gameObject.SetActive(false); // Hide the text
    }
}
