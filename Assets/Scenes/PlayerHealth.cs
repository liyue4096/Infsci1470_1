using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private Animator _animator;
    public bool isDie = false;
    public int hp = 2;
    public bool isHoldingShield = false;
    public bool isDefending = false;
    public GameObject shield; // Assign this in the Inspector
    public Text healthDisplay; // Reference to the Text UI component


    public void HandlePlayerDamage()
    {
        if (isHoldingShield && isDefending)
        {
            Debug.Log("defend successed!");
            return;
        }

        hp--;
        UpdateHealthDisplay();

        if (hp <= 0) { Die(); }
        
    }

    public void Die()
    {
        isDie = true;
        _animator.SetBool("isDie", true);
        // Any additional death logic (e.g., playing a death animation)
        Debug.Log(gameObject.name + " has died.");
    }

    void Start()
    {
        _animator = GetComponent<Animator>();
        UpdateHealthDisplay();
    }

    private void UpdateHealthDisplay()
    {
        if (healthDisplay != null)
            healthDisplay.text = "HP: " + hp; // Update the text to reflect current HP
    }

    private void Update()
    {
        if (hp <= 0)
        {
            Die();
        }
        isHoldingShield = shield.activeInHierarchy;
        isDefending = _animator.GetBool("isDefend");
    }

}
